import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundPageComponent } from './modules/shared/components/not-found-page/not-found-page.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'api', pathMatch: "full"
  },
  {
    path: 'api', loadChildren: () =>
      import('./modules/public/public.module').then(x => x.PublicModule),
  },
  {
    path: 'main', loadChildren: () =>
      import('./modules/private/components/layout.module').then(x => x.LayoutModule),
  },
  {
    path: '**',
    component: NotFoundPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
