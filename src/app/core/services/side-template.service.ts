import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SideTemplateService {

  constructor() {
  }

  public subject = new Subject<boolean>();


  SET(value: boolean): void {
    this.subject.next(value);
  }

  GET(): Observable<boolean> {
    return this.subject.asObservable();
  }
}



