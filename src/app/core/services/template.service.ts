import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { MenuModel } from '../models/menu.model';
import { ConstEnum } from 'src/app/help/enums/const.enum';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {

  constructor() {
  }

  public subject = new Subject<boolean>();

  static getMenu(): MenuModel[] {
    const permissions = localStorage.getItem(ConstEnum.PermissionClams);
    let menu = MENU;
    // for (let item of permissions) {
    //   menu = menu.map(x => {
    //     if (CheckPermission(x.permissions)) {
    //       return x;
    //     }
    //   })
    // }
    return MENU;
  }

  SET(value: boolean): void {
    this.subject.next(value);
  }

  GET(): Observable<boolean> {
    return this.subject.asObservable();
  }
}

const MENU: MenuModel[] = [
  { icon: 'group', title: 'User_management', link: 'user'},
  { icon: 'location_on', title: 'Branch_management', link: 'state'},
  { icon: 'call_merge', title: 'Attach_management', link: 'attach' },
  { icon: 'security', title: 'Role_management', link: 'role' }
];

