import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { UserModel } from '../models/user.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseService<UserModel>{

  constructor(public http: HttpClient) {
    super('user', http);
  }

  profile(): Observable<UserModel> {
    return this.http.get<UserModel>(`${environment.apiUrl}auth/profile`)
  }
}
