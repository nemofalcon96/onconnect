import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserModel } from "../models/user.model";
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators'
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private token = null;
    constructor(private http: HttpClient, private router: Router) {
    }

    register(user: UserModel): Observable<UserModel> {
        return this.http.post<UserModel>(`${environment.apiUrl}auth/register`, user);
    };


    login(user: UserModel): Observable<{ token: string }> {
        return this.http.post<{ token: string }>(`${environment.apiUrl}auth/login`, user)
            .pipe(
                tap(
                    ({ token }) => {
                        localStorage.setItem('auth-token', token)
                    }),
            )
    }

    google(): Observable<UserModel> {
        return this.http.get<UserModel>(`${environment.apiUrl}auth/google`);
    };

    isAuthenticated(): boolean {
        return !!this.token
    }

    logout() {
        localStorage.clear();
        this.router.navigate(['/'])
    }
}     
