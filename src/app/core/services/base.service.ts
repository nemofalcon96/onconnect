import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
export class BaseService<T> {

  protected baseUrl = '';

  constructor(protected controller: string, protected http: HttpClient) {
    this.baseUrl = `${environment.apiUrl}${controller}`;
  }

  getAll(): Observable<T[]> {
    return this.http.get<T[]>(this.baseUrl);
  }

  getById(id: string): Observable<T> {
    const url = `${this.baseUrl}/${id}`;
    return this.http.get<T>(url);
  }

  create(model: T): Observable<T> {
    return this.http.post<T>(this.baseUrl, model);
  }

  update(model: T): Observable<T> {
    // @ts-ignore
    const url = `${this.baseUrl}/${model.id}`;
    return this.http.put<T>(url, model);
  }

  save(model: T): Observable<T> {

    console.log(model);
    // @ts-ignore
    if (model.id) {
      return this.update(model);
    } else {
      return this.create(model);
    }
  }
}
