export class MenuModel {
  icon?: string;
  title?: string;
  link?: string;
}
