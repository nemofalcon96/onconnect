import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { SharedPrivateModule } from '../../shared/shared-private.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';


@NgModule({
  declarations: [MainComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    SharedModule,
    SharedPrivateModule
  ]
})
export class MainModule { }
