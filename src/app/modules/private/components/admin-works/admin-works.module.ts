import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminWorksRoutingModule } from './admin-works-routing.module';
import { AdminWorksComponent } from './admin-works.component';
import { SharedModule } from 'src/app/modules/shared/shared.module';


@NgModule({
  declarations: [AdminWorksComponent],
  imports: [
    CommonModule,
    AdminWorksRoutingModule,
    SharedModule
  ]
})
export class AdminWorksModule { }
