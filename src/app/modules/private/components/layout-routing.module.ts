import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
  {
    path: '', component: LayoutComponent, children: [
      {
        path:"", redirectTo:"registration"
      },
      {
        path: 'registration', loadChildren: () => import('./main/main.module').then(x => x.MainModule),
      },
      {
        path: 'admin-works', loadChildren: () => import('./admin-works/admin-works.module').then(x => x.AdminWorksModule),
      },    {
        path: 'user-list', loadChildren: () => import('./user/user.module').then(x => x.UserModule),
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
