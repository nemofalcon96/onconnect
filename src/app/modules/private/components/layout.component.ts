import { Component, OnInit } from '@angular/core';
import { TemplateService } from 'src/app/core/services/template.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  isExpanding = false;
  constructor(public templateService: TemplateService) { }
  ngOnInit() {
    this.templateService.GET().subscribe(res => {
      this.isExpanding = res;
    });
  }


}
