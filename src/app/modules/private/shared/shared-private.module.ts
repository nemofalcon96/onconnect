import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationToolbarComponent } from './components/registration-toolbar/registration-toolbar.component';
import { SharedModule } from '../../shared/shared.module';
import { MainSidenavComponent } from './components/main-sidenav/main-sidenav.component';
import { MatListModule } from '@angular/material/list';
import { RouterModule } from '@angular/router';
import { MainSidenavCloseComponent } from './components/main-sidenav-close/main-sidenav-close.component';



@NgModule({
  declarations: [RegistrationToolbarComponent, MainSidenavComponent, MainSidenavCloseComponent],
  imports: [
    CommonModule,
    MatListModule,
    RouterModule,
    SharedModule
  ],
  exports:[
    RegistrationToolbarComponent,
    MainSidenavComponent,
    MainSidenavCloseComponent
  ]
})
export class SharedPrivateModule { }
