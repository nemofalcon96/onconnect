import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainSidenavCloseComponent } from './main-sidenav-close.component';

describe('MainSidenavCloseComponent', () => {
  let component: MainSidenavCloseComponent;
  let fixture: ComponentFixture<MainSidenavCloseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainSidenavCloseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainSidenavCloseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
