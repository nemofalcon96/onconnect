import { Component, OnInit } from '@angular/core';
import { MenuModel } from 'src/app/core/models/menu.model';
import { SideTemplateService } from 'src/app/core/services/side-template.service';
import { TemplateService } from 'src/app/core/services/template.service';
import { PermissionEnum } from 'src/app/help/enums/permission.enum';
import { RouteEnum } from 'src/app/help/enums/route.enum';

@Component({
  selector: 'app-main-sidenav-close',
  templateUrl: './main-sidenav-close.component.html',
  styleUrls: ['./main-sidenav-close.component.scss']
})
export class MainSidenavCloseComponent implements OnInit {

  isExpanded = true;
  branchPanel = false;
  reportPanel = false;
  auditPanel = false;
  menu: MenuModel[] = [];
  routeEnum = RouteEnum;
  permissionEnum = PermissionEnum;
  constructor(public templateService: SideTemplateService) { }

  ngOnInit() {
      this.templateService.GET().subscribe(res => {
          this.isExpanded = res;
      });
      this.menu = TemplateService.getMenu();
  }
  onClick() {
    this.templateService.SET(this.isExpanded);
}
}
