import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { MenuModel } from 'src/app/core/models/menu.model';
import { SideTemplateService } from 'src/app/core/services/side-template.service';
import { TemplateService } from 'src/app/core/services/template.service';
import { PermissionEnum } from 'src/app/help/enums/permission.enum';
import { RouteEnum } from 'src/app/help/enums/route.enum';

@Component({
  selector: 'app-main-sidenav',
  templateUrl: './main-sidenav.component.html',
  styleUrls: ['./main-sidenav.component.scss']
})
export class MainSidenavComponent implements OnInit {


  isExpanded = true;
  branchPanel = false;
  reportPanel = false;
  auditPanel = false;
  menu: MenuModel[] = [];
  routeEnum = RouteEnum;
  permissionEnum = PermissionEnum;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  obs!: Observable<any>;
  Orders = [
    { id: 1, qaror: '1-2676-2021/5677', yuklangan: '12.02.2021', tasdiqlangan: '12.02.2021', nomi: '10- Сайлов комиссиясининг харакатлари низолаш тугрисида' },
    { id: 2, qaror: '1-2676-2021/5677', yuklangan: '12.02.2021', tasdiqlangan: '12.02.2021', nomi: '10- Сайлов комиссиясининг харакатлари низолаш тугрисида' },
    { id: 3, qaror: '1-2676-2021/5677', yuklangan: '12.02.2021', tasdiqlangan: '12.02.2021', nomi: '10- Сайлов комиссиясининг харакатлари низолаш тугрисида' },
    { id: 4, qaror: '1-2676-2021/5677', yuklangan: '12.02.2021', tasdiqlangan: '12.02.2021', nomi: '10- Сайлов комиссиясининг харакатлари низолаш тугрисида' },
    { id: 5, qaror: '1-2676-2021/5677', yuklangan: '12.02.2021', tasdiqlangan: '12.02.2021', nomi: '10- Сайлов комиссиясининг харакатлари низолаш тугрисида' },
    { id: 6, qaror: '1-2676-2021/5677', yuklangan: '12.02.2021', tasdiqlangan: '12.02.2021', nomi: '10- Сайлов комиссиясининг харакатлари низолаш тугрисида' },
    { id: 7, qaror: '1-2676-2021/5677', yuklangan: '12.02.2021', tasdiqlangan: '12.02.2021', nomi: '10- Сайлов комиссиясининг харакатлари низолаш тугрисида' },
    { id: 8, qaror: '1-2676-2021/5677', yuklangan: '12.02.2021', tasdiqlangan: '12.02.2021', nomi: '10- Сайлов комиссиясининг харакатлари низолаш тугрисида' },
    { id: 9, qaror: '1-2676-2021/5677', yuklangan: '12.02.2021', tasdiqlangan: '12.02.2021', nomi: '10- Сайлов комиссиясининг харакатлари низолаш тугрисида' },
    { id: 10, qaror: '1-2676-2021/5677', yuklangan: '12.02.2021', tasdiqlangan: '12.02.2021', nomi: '10- Сайлов комиссиясининг харакатлари низолаш тугрисида' },
  ];
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>(this.Orders);
  constructor(public templateService: SideTemplateService, private changeDetectorRef: ChangeDetectorRef) { }
  ngOnInit() {
    this.templateService.GET().subscribe(res => {
      this.isExpanded = res;
    });
    this.menu = TemplateService.getMenu();

    this.changeDetectorRef.detectChanges();
    this.dataSource.paginator = this.paginator;
    this.obs = this.dataSource.connect();
  }


}
