import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationToolbarComponent } from './registration-toolbar.component';

describe('RegistrationToolbarComponent', () => {
  let component: RegistrationToolbarComponent;
  let fixture: ComponentFixture<RegistrationToolbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrationToolbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
