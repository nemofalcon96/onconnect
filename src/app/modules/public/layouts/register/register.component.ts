import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm!: FormGroup;
  isLoading = false;
  hide = {
    password: true,
  };
  constructor(
    public fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private toast: ToastrService) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  // initialize login form
  initForm(): void {
    this.registerForm = this.fb.group({
      email: ['', [Validators.required,  Validators.minLength(5), Validators.email, Validators.maxLength(30)]],
      password: ['', [Validators.required,Validators.minLength(5), Validators.maxLength(20)]],
    });
  }

  public hasError = (controlName: string, errorName: string) => {
    const control = this.registerForm.controls[controlName];
    return control ? control.hasError(errorName) && this.registerForm.controls[controlName].touched : false;
  };

  onSubmit(): void {
    this.isLoading = true;
    this.authService.register(this.registerForm.value)
      .subscribe(result => {
        this.router.navigate(['/api/login'])
      }, () => {
        this.isLoading = false;
      });
  }

  password() {
    this.hide.password = !this.hide.password;
  }
}
