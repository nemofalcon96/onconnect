import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/core/services/auth.service';


@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  isLoading = false;
  hide = {
    password: true,
  };
  constructor(
    public fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private toast: ToastrService) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  // initialize login form
  initForm(): void {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.minLength(5), Validators.email, Validators.maxLength(30)]],
      password: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(20)]],
    });
  }

  public hasError = (controlName: string, errorName: string) => {
    const control = this.loginForm.controls[controlName];
    return control ? control.hasError(errorName) && this.loginForm.controls[controlName].touched : false;
  };

  onSubmit(): void {
    this.isLoading = true;
    this.authService.login(this.loginForm.value)
      .subscribe(result => {
        this.router.navigate(['/main'])
      }, () => {
        this.isLoading = false;
      });
  }

  password() {
    this.hide.password = !this.hide.password;
  }
}
