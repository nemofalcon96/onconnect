import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-navbar-web',
  templateUrl: './navbar-web.component.html',
  styleUrls: ['./navbar-web.component.scss'],
})
export class NavbarWebComponent implements OnInit, OnDestroy {
  private subs = new SubSink();
  // Logo url
  logo: string;
  // Show mobile menu toggler
  showMobileMenu = false;
  // Is brand or blog selected boolean
  brand = false;
  blog = false;

  constructor(private router: Router, private route:ActivatedRoute) {}

  ngOnInit(): void {
    this.logo = 'assets/img/mnWho-logo-violet.svg';
    // this.watchRouterChanges();
    // this.checkRoute();
  }

  // Check current route anad compare to the hide navs
  checkRoute() {
    // Get first element of current route after /
    const currentRoute = this.router.url.split('/')[1];

    // Check if brand is selected
    const brandRoutes = ['brands', 'brand-how-to'];
    this.brand = brandRoutes.indexOf(currentRoute) !== -1;

    // Check if blog is selected
    const blogRoutes = ['blog', 'career-detail' ];
    this.blog = blogRoutes.indexOf(currentRoute) !== -1;

    // Check which route is selected
    if (this.brand) {
      this.logo = 'assets/img/mnWho-logo-green.svg';
    } else if (this.blog) {
      this.logo = 'assets/img/mnWho-logo.svg ';
    } else {
      this.logo = 'assets/img/mnWho-logo-violet.svg';
    }
  }

  // Keep watching route changes on activated module
  watchRouterChanges() {
    this.subs.sink = this.router.events.subscribe(() => {
      this.checkRoute();
    });
  }

  // Unsubscribe from subscriptions on destroy
  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
