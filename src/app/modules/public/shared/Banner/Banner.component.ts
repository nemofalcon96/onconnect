import { Component, Input, OnInit } from '@angular/core';


@Component({
    selector: 'Banner',
    templateUrl: './Banner.component.html',
    styleUrls: ['./Banner.component.scss']
})
export class Banner implements OnInit {
    constructor() { }
    ngOnInit(): void {
    }

     
    @Input() title: String
    @Input() text: String
    @Input() img: String
    @Input() type: String
}
