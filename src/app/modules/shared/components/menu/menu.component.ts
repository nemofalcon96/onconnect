import { Component, OnInit, ViewChild } from '@angular/core';
import { MatMenu } from "@angular/material/menu";
import { UserModel } from 'src/app/core/models/user.model';
import { Router } from "@angular/router";
// import { UserService } from 'src/app/core/services/user.service';
import { TemplateService } from 'src/app/core/services/template.service';
import { ConstEnum } from 'src/app/help/enums/const.enum';
import { LanguageFunction } from 'src/app/help/functions/language.function';
import { TranslateService } from '@ngx-translate/core';

@Component({ selector: 'app-menu', templateUrl: './menu.component.html', styleUrls: ['./menu.component.scss'], exportAs: 'menuInOtherComponent' })
export class MenuComponent implements OnInit {
    url: any = '';
    isExpanded = false;
    language!: string ;
    user: UserModel = new UserModel();
    @ViewChild(MatMenu, { static: true }) menu!: MatMenu;

    constructor(
        public templateService: TemplateService,
        public router: Router,
        public translate: TranslateService,
    ) { }

    ngOnInit() {
        this.templateService.SET(false);
        let lang = localStorage.getItem(ConstEnum.LanguageKey) ? localStorage.getItem(ConstEnum.LanguageKey) : ConstEnum.EnglishCode;
        this.setLanguage(String(lang));
        this.getUser();
    }

    onClick() {
        this.templateService.SET(this.isExpanded);
    }
    setLanguage(language: string) {
        this.language = language;
        LanguageFunction(this.translate, language);
    }

    getUser() {
        // this.userService.getCurrentUser().subscribe(() => {
        //     // this.userService.subject.subscribe(res => {
        //     //     this.user = res;
        //     // });
        // });
    }

    logout() {
        this.router.navigate(['/'])
        // this.authService.logout();
    }

}
