import { Component, Input, OnInit } from '@angular/core';
import { MatMenu } from "@angular/material/menu";
import { TranslateService } from '@ngx-translate/core';
import { TemplateService } from 'src/app/core/services/template.service';
import { ConstEnum } from 'src/app/help/enums/const.enum';
import { LanguageFunction } from 'src/app/help/functions/language.function';

@Component({ selector: 'app-toolbar', templateUrl: './toolbar.component.html', styleUrls: ['./toolbar.component.scss'] })
export class ToolbarComponent implements OnInit {
    isExpanded = false;
    language!: string;
    @Input() matMenu!: MatMenu;

    constructor(public templateService: TemplateService, public translate: TranslateService) { }

    ngOnInit() {
        this.templateService.SET(false);
        const lang = localStorage.getItem(ConstEnum.LanguageKey) ? localStorage.getItem(ConstEnum.LanguageKey) : ConstEnum.EnglishCode;
        this.setLanguage(String(lang));
    }

    onClick() {
        this.templateService.SET(this.isExpanded);
    }

    setLanguage(language: string) {
        this.language = language;
        LanguageFunction(this.translate, language);
    }
}
