import { Component, OnInit } from '@angular/core';
import { RouteEnum } from 'src/app/help/enums/route.enum';
import { PermissionEnum } from 'src/app/help/enums/permission.enum';
import { MenuModel } from 'src/app/core/models/menu.model';
import { TemplateService } from 'src/app/core/services/template.service';

@Component({
    selector: 'app-sidenav',
    templateUrl: './sidenav.component.html',
    styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

    isExpanded = true;
    branchPanel = false;
    reportPanel = false;
    auditPanel = false;
    menu: MenuModel[] = [];
    routeEnum = RouteEnum;
    permissionEnum = PermissionEnum;
    constructor(public templateService: TemplateService) { }

    ngOnInit() {
        this.templateService.GET().subscribe(res => {
            this.isExpanded = res;
        });
        this.menu = TemplateService.getMenu();
    }

}
