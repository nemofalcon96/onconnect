import { FormGroup } from '@angular/forms';

export class AppValidators {
  public static matchingPasswords = (
    control: FormGroup
  ): { matchingPasswords: boolean } => {
    if (
      control.get('confirm_new_password').value !==
      control.get('new_password').value
    ) {
      return { matchingPasswords: true };
    }
  };

  public static url = (control: FormGroup): { urlNotValid: boolean } => {
    const validator = new RegExp(
      /^(?:(?:https):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i
    );

    if (validator.test(control.value)) return null;
    if (!validator.test(control.value)) return { urlNotValid: true };
  };
}
