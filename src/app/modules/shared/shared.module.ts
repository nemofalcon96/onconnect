import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MenuComponent } from './components/menu/menu.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { MaterialModule } from './modules/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    NotFoundPageComponent,
    MenuComponent,
    ToolbarComponent,
    NotFoundPageComponent,
    SidenavComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    TranslateModule,
  ],
  exports: [
    CommonModule,
    MenuComponent,
    NotFoundPageComponent,
    SidenavComponent,
    ToolbarComponent,
    MaterialModule,
    TranslateModule,
  ],
  providers: [DatePipe, DecimalPipe,],
})
export class SharedModule { }
