import { ConstEnum } from '../enums/const.enum';
import { TranslateService } from '@ngx-translate/core';

export function LanguageFunction(translate: TranslateService, lang: string) {
  localStorage.setItem(ConstEnum.LanguageKey, lang);
  translate.setDefaultLang(lang);
  // return lang === ConstEnum.EnglishCode ? 'flag-icon flag-icon-gb' : 'flag-icon flag-icon-my';
}
