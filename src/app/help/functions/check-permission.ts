import { ConstEnum } from '../enums/const.enum';

export function CheckPermission(items: number[]): boolean {
	let permissions = JSON.parse(localStorage.getItem(ConstEnum.PermissionClams));
	if (items && permissions) {
		for (let item of items) {
			if (permissions.find((x: number) => x === item)) {
				return true;
			}
		}
	}
	return false;
}