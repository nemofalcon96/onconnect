export function FilterFunction(value: string, objects: any[]) {
	if (objects) {
		const properties = Object.getOwnPropertyNames(objects[0]).filter(y => y !== 'id');

		return objects.filter(object => {
			for (const property of properties) {
				if (object[property]) {
					try{
						const result = object[property].toUpperCase().includes(value.toUpperCase());
						if (result) {
							return object;
						}
					}catch(e){}
					
				}
			}
		});
	}
}