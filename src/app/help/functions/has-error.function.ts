import { FormGroup } from '@angular/forms';

export function HasErrorFunction(form: FormGroup, controlName: string, errorName: string): boolean {
	const control = form.controls[controlName];
	return control ? control.hasError(errorName) && control.touched : false;
}
