import { ConstEnum } from "../enums/const.enum";
import * as jwt_decode from 'jwt-decode';

export function PayladerFunction(value: string): string {
    const decode = jwt_decode(localStorage.getItem(ConstEnum.Permission));
    return decode[value];
}
