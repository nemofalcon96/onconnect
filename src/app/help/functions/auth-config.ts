import { AuthConfig } from 'angular-oauth2-oidc';
import { environments } from 'src/app/environments';

export const authConfig: AuthConfig = {
  // Url of the Identity Provider
  issuer: environments.issueUrl,
  // URL of the SPA to redirect the user to after login
  redirectUri: `${window.location.origin}${environments.API_ACTION}/api`,

  silentRefreshRedirectUri: `${window.location.origin}${environments.API_ACTION}/api`,

  // The SPA's id. The SPA is registerd with this id at the auth-server
  // clientId: 'server.code',
  clientId: 'admin-console-client',

  // Just needed if your auth server demands a secret. In general, this
  // is a sign that the auth server is not configured with SPAs in mind
  // and it might not enforce further best practices vital for security
  // such applications.
  // dummyClientSecret: 'secret',

  responseType: 'code',

  // set the scope for the permissions the client should request
  // The first four are defined by OIDC.
  // Important: Request offline_access to get a refresh token
  // The api scope is a usecase specific one
  scope: 'openid profile email offline_access admin.console.api',

  silentRefreshTimeout: 0, // For faster testing
  // timeoutFactor: 0.75, // For faster testing
  sessionChecksEnabled: false,
  showDebugInformation: true, // Also requires enabling "Verbose" level in devtools
  clearHashAfterLogin: true, // https://github.com/manfredsteyer/angular-oauth2-oidc/issues/457#issuecomment-431807040
  //Todo: fkurbonov
  strictDiscoveryDocumentValidation: false,
  // Not recommented:
  // disablePKCI: true,

  // postLogoutRedirectUri: 'localhost:4200'
  postLogoutRedirectUri: environments.logoutUrl,
};
