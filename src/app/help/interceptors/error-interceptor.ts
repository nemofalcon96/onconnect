import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ErrorInterceptorService implements HttpInterceptor {

  constructor(private toastService: ToastrService,
    private router: Router) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            if (event.status >= 200 || 299 <= event.status) {
              if (event.body.message) {
                this.toastService.success(event.body.message)
              }
            }
            if (event.status >= 300 || 399 <= event.status) {
              const errorMessage = event.body?.message;
              errorMessage.forEach((message: string) => {
                this.toastService.warning(message);
              });
            }
          }
          return event;
        }),
        catchError((error: HttpErrorResponse) => {
          this.handleError(error);
          return throwError('error');
        })
      );
  }

  handleError(err: HttpErrorResponse): any {
    const errorMessage = err.error.message;
    this.toastService.error(errorMessage);
  }
}
