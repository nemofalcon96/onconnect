export enum PermissionEnum {
    StateWrite = 10,
    StateRead = 20,
    DistrictWrite = 30,
    DistrictRead = 40,
    GroupWrite = 50,
    GroupRead = 60,
    UserWrite = 70,
    UserRead = 80,
    RoleWrite = 90,
    RoleRead = 100,
    AnaliticsWrite = 110,
    AnaliticsRead = 120,
    MonitoringAdmin = 130,
    MonitoringEditor = 140,
    MonitoringViewer = 180,
    Logging = 150,
    AuditTrailing = 160,
    NetworkTrailing = 170,
    ReportWrite = 190,
    ReportRead = 200
  }
  