export enum StatusEnum {
	creating = 10,
	created = 20,
	return = 30,
	not_verified = 40,
	disable = 50,
	enable = 60,
	disabling = 70,
	enabling = 80,
	approve = 90,
	updating = 100,
	suspend = 110,
	terminate = 120,
	terminating = 130,
}