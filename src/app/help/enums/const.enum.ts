export enum ConstEnum {
  LanguageKey = 'lang-key',
  MalayCode = 'my',
  EnglishCode = 'en',
  Permission = 'permission',
  Permissions = 'permissions',
  PermissionClams = 'permission_clams',
  AccessToken = 'access_token',
  InstabiToken = 'InstabiToken',
  Bi = 'bi',
  RoleType = 'roleType',
  PageIndex = 'pageIndex',
  PageSize='pageSize'
}
