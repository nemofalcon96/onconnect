import { Pipe, PipeTransform } from "@angular/core";
import { UserModel } from '../../core/models/user.model';

@Pipe({
    name: 'short_name'
})
export class ShortName implements PipeTransform {
    transform(user: UserModel): string {
        return `${user.first_name} ${user.last_name}`
    }
}