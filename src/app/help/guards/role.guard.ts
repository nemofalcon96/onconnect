import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { ConstEnum } from '../enums/const.enum';
import { OAuthService } from 'angular-oauth2-oidc';
import { NgxPermissionsService } from 'ngx-permissions';
import { CheckPermission } from '../functions/check-permission';
import { AuthService } from 'src/app/core/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  constructor(public oauthService: OAuthService,
    public authService: AuthService,
    public permissionsService: NgxPermissionsService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    // const permissions = JSON.parse(localStorage.getItem(ConstEnum.PermissionClams));
    // console.log(permissions);
    // if (permissions.length !== 0) {
    //   for (let item of route.data.permissions) {
    //     if (permissions.find(x => x === item)) {
    //       return true;
    //     }
    //   }
    // }

    let result = CheckPermission(route.data.permissions);
    if (result) {
      return true;
    } else {
      // this.authService.logout();
      return false;
    }

    return true;
  }
}
