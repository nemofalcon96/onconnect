import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map, filter, tap } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services/auth.service';

@Injectable({
	providedIn: 'root'
})
export class AuthGuard implements CanActivate {

	private isAuthenticated: boolean;

	constructor(private authService: AuthService) {
		this.authService.isAuthenticated$.subscribe(i => this.isAuthenticated = i);
	}

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot,
	): Observable<boolean> {
		console.log(state.url);
		return this.authService.isDoneLoading$
			.pipe(filter(isDone => isDone))
			.pipe(tap(_ => this.isAuthenticated || this.authService.login(state.url)))
			.pipe(map(_ => this.isAuthenticated));
	}
}
